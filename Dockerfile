# CentOS 7 + NodeJS + dev tools

FROM node:7

MAINTAINER Andre Fernandes <andre.oliveira@bcb.gov.br>

RUN npm install --unsafe-perm -g strongloop && \
    npm install -g nodemon && \
    npm cache clean
